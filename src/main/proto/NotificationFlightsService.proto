syntax = "proto3";

option java_multiple_files = true;

package com.accelaero.aerobridge.aeromart.gateway;

import "google/api/annotations.proto";
import "Common.proto";


service NotificationFlightsService {

  rpc searchFlights (SendNotificationFlightsSearchRequest) returns (SendNotificationFlightsSearchResponse) {
    option (google.api.http) = {
      post: "/aeromart/searchFlights"
      body: "*"
    };
  };

  rpc searchOrders (OrderSearchRequest) returns (OrderSearchResponse) {
    option (google.api.http) = {
      post: "/aeromart/searchOrders"
      body: "*"
    };
  };
}


message SendNotificationFlightsSearchRequest {
  //todo re verify carrier code and flight number field.
  repeated string carrierCode = 1; // [G9] : for aeroMart send only one value
  repeated string flightNumbers = 2; // [G9123, G9345, E5337] : UI change to allow flight no with designator
  repeated string segmentCodes = 3; // [SHJ/TVM, DEL/SHJ, DEL/SHJ]
  DateRange flightDateRange = 4;
  DateRange alertDateRange = 5;
  string specifyDelay = 6;  //DD:HH:MM
  AnciReprotectStatus anciReprotectStatus = 7;
  ConnectionCriteria connectionCriteria = 8;

  FlightSearchSortFields sortField = 9;
  SortOrder sortOrder = 10;
  string pageNumber = 11;
  string pageSize = 12;
}

//FLIGHT SEARCH REQUEST
message SendNotificationFlightsSearchResponse {
  repeated ScheduleDetails ScheduleDetailsList = 1;

}

message ScheduleDetails {
  //todo re verify carrier code and flight number field.
  string carrierCode = 1; //G9
  string flightNumber = 2; //G9337
  string segmentCode = 3; //SHJ/BAH
  int32 numberOfFlights = 4;

  DateRange flightDateRange = 5; // Start Date & End Date
  string departureTimeZulu = 6; // ETD: hh:mm:ss or empty
  string departureTimeLocal = 7; // ETD: hh:mm:ss or empty
  string arrivalTimeZulu = 8; // ETA: hh:mm:ss or empty
  string arrivalTimeLocal = 9; // ETA: hh:mm:ss or empty

  repeated FlightDetails flightDetailsList= 10;
}

message FlightDetails {
  string carrierCode = 1; //G9
  string flightNumber = 2; //G9337
  string localDepartureDateTime = 3; //2021-12-10T18:40:00
  string zuluDepartureDateTime =4;  //2021-12-10T18:40:00
  string segmentCode = 5; //SHJ/BAH

  // todo TBD: all below details at flight level in aeroMart, but reprotect has clubbed flights or schedules in result
  FlightStatus flightStatus = 6; //enum ACT/INA
  FlightLoadDetails flightLoadDetails = 7;
  string alertDate = 8;  //dd/MM/yyyy or Empty
  FlightConnectionDetails flightConnectionDetails = 9;
}

message FlightLoadDetails {
  repeated PaxQuantity paxQuantity = 1;
  int32 totalPnrCount = 2;
}

message FlightConnectionDetails {
  int32 inboundConnections = 1;
  int32 outboundConnections = 2;
}
//END FLIGHT SEARCH REQUEST

//START ORDER SEARCH REQUEST
message OrderSearchRequest {
  repeated string orderIds = 1;
  repeated string passengerNames = 2;
  repeated string carrierCodes = 3; // [G9, 3O]
  repeated string flightNumbers = 4; // // [G9123, G9345, E5337] : UI change to allow flight no with designator
  DateRange flightDateRange = 5;
  TimeRange flightTimeRange = 6;
  repeated string flightDates = 7;
  repeated string countries = 8; //["IN", "AE"]
  repeated string nationalities = 9; //["IN", "AE"]
  repeated Channel channels = 10;
  repeated string agentNames = 11;
  OrderSearchSortFields sortField = 12;
  SortOrder sortOrder = 13;
  string pageNumber = 14;
  string pageSize = 15;
  repeated FlightSegment flightSegment = 16;
}

message OrderSearchResponse {
  repeated OrderSummary matchingOrders = 1;
  int64 totalMatchingOrders = 2;
}

message OrderSummary {
  string pnr = 1;
  string orderId = 2;
  string onHoldReleaseTime = 3;
  ContactDetails contactDetails = 4;
  repeated PaxQuantity paxQuantities = 5;
  Agency agency = 6;
  Preferences preferences = 7;
  repeated FlightSegment flightSegments = 8;
  Status status = 9;
  repeated PaxDetails paxDetails = 10;
  repeated PaxFlightSegment paxFlightSegment = 11;
}

message FlightSegment {
  repeated FlightDesignator flightNumber = 1;
  string segmentCode = 2;
  string origin = 3;
  string destination = 4;
  string localDepartureDateTime = 5;
  string zuluDepartureDateTime = 6;
  string localArrivalDateTime = 7;
  string zuluArrivalDateTime = 8;
  string flightSegmentRPH = 9;
  PaxFlightSegmentStatus segmentStatus = 10;
}

message PaxFlightSegment {
  string paxRPH = 1;
  string flightSegmentRPH = 2;
  string bookingClassCode = 3;
  string cabinClassCode = 4;
  SegmentCategory segmentCategory = 5;
}

message Status {
  OrderStatus orderStatus = 1;
}

message FlightDesignator {
  string airlineDesignator = 1;
  string flightNumber = 2;
  string operationalSuffix = 3;
  FlightCarrierType flightCarrierType = 4;
}

message PaxDetails {
  PaxType paxType = 1;
  string paxRPH = 2;
  string accompaniedAdultRPH = 3;
  string title = 4;
  string firstName = 5;
  string lastName = 6;
  string loyaltyId = 7;
  Nationality nationality = 8;
  repeated string flightRPH = 9; // If the list is empty -> applicable for all flight segments
}

message Preferences {
  Language language = 1;
}

message Agency {
  string agencyCode = 1;
  string agencyName = 2;
}

message ContactDetails {
  string title = 1;
  string firstName = 2;
  string lastName = 3;
  repeated PhoneNumber phoneNumbers = 4;
  Address address = 5;
  repeated EmailAddress emailAddresses = 6;
  Nationality nationality = 7;
}

message Address {
  string addressLine1 = 1;
  string addressLine2 = 2;
  string postalCode = 3;
  string city = 4;
  string state = 5;
  Country country = 6;
}

//END ORDER SEARCH REQUEST